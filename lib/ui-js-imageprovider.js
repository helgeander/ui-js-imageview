
class UiJsImageprovider {

  constructor(url) {
    if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
      url = url.indexOf('/') === 0
        ? `${location.protocol}${location.host}${url}`
        : `${location.toString()}${url}`;
    }

    this.url =  new URL(url);
    this.searchParams = Object.fromEntries(this.url.searchParams);
    this.url.search = '';
  }

  getUrl(file, page, width, height, contentType) {
    const searchParams = Object.assign(this.searchParams, {
      file: file,
      page: page,
      width: width,
      height: height,
      contentType: contentType
    });
    const search = Object
      .keys(searchParams)
      .reduce((acc, cur) => acc.concat(searchParams[cur] ? `${cur}=${encodeURIComponent(searchParams[cur])}&` : ''), '?');
    return `${this.url.href}${search}`;
  }

  async getImage(file, page, width, height, contentType) {
    const url = this.getUrl(file, page, width, height, contentType);
    const image = new Image();
    image.src = url;
    await image.decode();
    return image;
  }

}

export default UiJsImageprovider;