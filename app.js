import { template } from './lib/ui-js-lib.js';
import UiJsImageprovider from './lib/ui-js-imageprovider.js';

import './src/ui-js-imageview.js';

const tpl = template`
  <ui-js-imageview
    file="1/honningurt.jpg"
    width="500"
    content-type="image/jpg"
    title="CTRL + Scroll to zoom">
  </ui-js-imageview>
`;

class AppView extends HTMLElement {
  constructor() {
    super();
    this.imgProvider = new UiJsImageprovider(`/img`);
  }

  async connectedCallback() {
    tpl().render(this);

    const imgView = this.querySelector('ui-js-imageview');
    imgView.ImageProvider = this.imgProvider;
  }
}

customElements.define('app-view', AppView);