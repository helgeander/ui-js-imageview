import { template } from '../lib/ui-js-lib.js';
import { fromEvent } from 'https://unpkg.com/rxjs@7.3.1/dist/esm5/internal/observable/fromEvent.js?module';
import { map } from 'https://unpkg.com/rxjs@7.3.1/dist/esm5/internal/operators/map.js?module';
import { debounceTime } from 'https://unpkg.com/rxjs@7.3.1/dist/esm5/internal/operators/debounceTime.js?module';

const tpl = template`
  <svg viewBox="0 0 2000 2000" xmlns="http://www.w3.org/2000/svg">
    <image/>
  </svg>
`;

class UiJsImageView extends HTMLElement {
  constructor() {
    super();

    this.zoom = 0.5;
  }

  async connectedCallback() {

    tpl().render(this);

    this.svg = this.querySelector('svg');
    this.svgImage = this.svg.querySelector('image');

    fromEvent(this, 'wheel')
      .pipe(map(ev => {
        if (ev.ctrlKey) {
          ev.preventDefault();
          this.wheelZoom(ev.deltaY);
        }
      }))
      .pipe(debounceTime(50))
      .subscribe(() => this.update());

      if (this.getAttribute('url'))
        this.load();
  }

  async getImage() {
    const image = new Image();
    image.src = url;
    return await image.decode();
  }

  async load() {
    const image = this.imageProvider
      ? await this.imageProvider.getImage(this.getAttribute('file'), 1, 1000)
      : await this.getImage(this.getAttribute('url'));

    this.imageWidth = image.width;
    this.imageHeigh = image.height;

    this.svgImage.setAttribute('width', image.width);
    this.svgImage.setAttribute('height', image.height);
    this.svgImage.setAttribute('href', image.src);

    this.update();
  }

  zoomIn() {
    const zoom = this.zoom / 0.95;
    if (zoom > 4)
      return;
    this.zoom = zoom;
  }

  zoomOut() {
    const zoom = this.zoom * 0.95;
    if (zoom < 0.1)
      return;
    this.zoom = zoom;
  }

  update() {
    const vw = this.imageWidth / this.zoom;
    const vh = this.imageWidth / this.zoom;
    this.svg.setAttribute('viewBox', `0 0 ${vw} ${vh}`);
  }

  wheelZoom(delta) {
    if (!delta) return;
    if (delta < 0)
      this.zoomIn();
    else
      this.zoomOut();
  }

  set ImageProvider(imageProvider) {
    this.imageProvider = imageProvider;
    this.load();
  }
}

customElements.define('ui-js-imageview', UiJsImageView);