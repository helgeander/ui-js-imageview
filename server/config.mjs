import convict from 'convict'

const config = convict({
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV"
  },
  azure: {
    key: {
      doc: "Azure storage access key",
      format: String,
      default: ''
    },
    account: {
      doc: "Account name",
      format: String,
      default: ''
    }
  }
});

// Load environment dependent configuration
const env = config.get('env');
config.loadFile('../config/' + env + '.json');

// Perform validation
config.validate({allowed: 'strict'});

export default config;