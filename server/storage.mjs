import { BlobServiceClient, StorageSharedKeyCredential } from '@azure/storage-blob'
import config from './config.mjs'


// A helper method used to read a Node.js readable stream into a Buffer
async function streamToBuffer(readableStream) {
  return new Promise((resolve, reject) => {
    const chunks = [];
    readableStream.on("data", (data) => {
      chunks.push(data instanceof Buffer ? data : Buffer.from(data));
    });
    readableStream.on("end", () => {
      resolve(Buffer.concat(chunks));
    });
    readableStream.on("error", reject);
  });
}

export async function getBlob(path) {
  const account = config.get('azure.account');
  const accountKey = config.get('azure.key');

  const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);

  const blobServiceClient = new BlobServiceClient(
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  );

  const containerName = `heland-dev`;
  const containerClient = blobServiceClient.getContainerClient(containerName);

  const blockBlobClient = containerClient.getBlockBlobClient(path);
  const downloadBlockBlobResponse = await blockBlobClient.download(0);

  return downloadBlockBlobResponse.readableStreamBody;
}
