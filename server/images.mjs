import sharp from 'sharp'

export function resize(stream, format, width, height) {
  let transform = sharp();

  if (format)
    transform = transform.toFormat(format);

  if (width || height)
    transform = transform.resize(width, height);

  return stream.pipe(transform);
}