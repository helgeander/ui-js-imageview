import express from 'express'
import fileupload from 'express-fileupload'
import { getBlob } from './storage.mjs'
import { resize } from './images.mjs';

const PORT = 8000;

function saveFile(file) {
  console.log(`upload ${file.name}`);
  return new Promise((resolve, reject) => {
      file.mv(`../uploads/${file.name}`, function(err) {
        if (err)
          reject(err);
        else
          resolve({
            name: file.name,
            mimetype: file.mimetype,
            size: file.size
          });
      });
  });
}

async function handleUploads(app) {
  app.post('/upload', async function(req, res) {
    console.log('upload started');
    if (!req.files || Object.keys(req.files).length === 0) {
      return res.status(400).send('No files were uploaded.');
    }

    let data = [];
    const files = Array.isArray(req.files.files) ? req.files.files : [req.files.files];

    for (const file of files) {
        try {
          const result = await saveFile(file);
          console.log(result);
          data.push(result);
        } catch (error) {
          return res.status(400).send(error);
        }
    }
    return res.send(data);
  });
}

function contentTypeToFormat(contentType) {
  if (contentType == 'image/png')
    return 'png';
  return 'jpg';
}

const app = express();

app.use(express.static('../'))

app.use('/img', async (req, res) => {
  const path = req.query.file
  const contentType = req.query.contentType
  const format = req.query.format || contentTypeToFormat(contentType)
  const width = req.query.width ? parseInt(req.query.width) : undefined;
  const height = req.query.height ? parseInt(req.query.height) : undefined;

  // Set the content-type of the response
  res.type(`image/${format || 'jpg'}`)

  const stream = await getBlob(path);

  // Get the resized image
  resize(stream, format, width, height).pipe(res);
})

app.use('/lib', express.static('../lib'))

app.use(fileupload());
handleUploads(app);

app.listen(PORT, function() {
  console.log('Express server listening on port ', PORT); // eslint-disable-line
});
